/**
 * KeeWeb plugin: SSH-Agnet
 * @author Marcel Feix
 * @license MIT
 */

let uninstalled = false;
const timeout = setTimeout(run, 2000);

function run() {

    //using eval so webpack is not Trying to find modules here
    var kdbxweb = eval('require')('kdbxweb')
    const AppModel = eval('require')('models/app-model')
    const Logger = eval('require')('util/logger')

    //SSh agent requirements
    var agent = require('sshpk-agent');
    var sshpk = require('sshpk');

    var client = new agent.Client({ socketPath: "/tmp/ssh-7CqAPC3IPcsS/agent.16008" });
    const FileReadTimeout = 500;

    const DebugMode = localStorage.keewebhttpDebug;

    uninstall = function () {
        uninstalled = true;
        removeEventListeners();
    };
    const logger = new Logger("SSH-Agent");

    addEventListeners();

    function addEventListeners() {
        AppModel.instance.files.on('add', fileOpened);
        AppModel.instance.files.on('remove', fileClosed);
    }

    function removeEventListeners() {
        AppModel.instance.files.off('add', fileOpened);
        AppModel.instance.files.off('remove', fileClosed);
    }

    function fileOpened(file) {
        logger.debug(file);
        logger.debug("File Opening");
        setTimeout(() => {
            readPemFiles(file);
        }, FileReadTimeout);
    }

    function fileClosed(file) {
        logger.debug("File Closing");
    }

    function readPemFiles(file) {
        if (uninstalled) {
            return;
        }

        for (const key in file.entryMap) {
            const attachts = file.entryMap[key].attachments;
            if (attachts.length > 0)
                for (const attachment of attachts) {
                    const key = sshpk.parsePrivateKey(Buffer.from(attachment.data));

                    client.addKey(key, function (err) {
                        logger.debug(err);
                    });
                }
        }
        client.listKeys(function (err, keys) {
            if (err)
                return;
            console.log(keys);
        });
    }

}

module.exports.uninstall = function () {
    uninstall()
};