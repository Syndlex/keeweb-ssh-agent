const path = require('path');

module.exports = {
    entry: './plugin.js',
    mode: "development",
    output: {
        filename: 'plugin.js',
        path: path.resolve(__dirname, 'SSH-Agnet'),
        libraryTarget: 'commonjs2'
    },
    module: {
        noParse: [/kdbxweb/, /models\/app-model/, /util\/logger/, /dtrace-provider*/],
    },
    node: {
        net: true,
    }
};